﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MetroFramework.Forms;
using Transitions;
using panelTransition.Panel;
using MetroFramework.Controls;
using MetroFramework;

namespace panelTransition
{
    public partial class secDash : MetroForm
    {
        secMem p1;
        public secDash()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            DateTime today = DateTime.Today;
            string datenow = today.ToString("D");
            string timenow = DateTime.Now.ToString("h:mm tt");
            dateLabel.Text = datenow;
            timeLabel.Text = timenow;
        }

        private void memberButton_Click(object sender, EventArgs e)
        {
            memberButton.Enabled = false;
            p1 = new secMem(this);
            p1.Closed += p1_Closed;
            p1.Shown += p1_Shown;
            p1.swipe(true);
            
            homeLinkButton.Visible = true;
            mainFormLabel.Text = "Member Profiling";
        }

        void p1_Closed(object sender, EventArgs e)
        {
            
        }

        void p1_Shown(object sender, EventArgs e)
        {

        }

        private void logoutLinkButton_Click(object sender, EventArgs e)
        {
            Application.Exit();

            /*
            if (MetroMessageBox.Show(this, "Are you sure you want logout?", "Confirm Logging Out!", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                loginForm n = new loginForm();
                this.Hide();
                n.Show();
            }
             */
        }

        private void homeLinkButton_Click(object sender, EventArgs e)
        {
            p1.swipe(false);
            homeLinkButton.Visible = false;
            memberButton.Enabled = true;
            mainFormLabel.Text = "Secretary Main Panel";
            
        }

    }

}
