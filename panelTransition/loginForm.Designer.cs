﻿namespace panelTransition
{
    partial class loginForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(loginForm));
            this.userText = new MetroFramework.Controls.MetroTextBox();
            this.passText = new MetroFramework.Controls.MetroTextBox();
            this.loginButton = new MetroFramework.Controls.MetroTextBox.MetroTextButton();
            this.exitButton = new MetroFramework.Controls.MetroButton();
            this.SuspendLayout();
            // 
            // userText
            // 
            this.userText.Anchor = System.Windows.Forms.AnchorStyles.None;
            // 
            // 
            // 
            this.userText.CustomButton.Image = null;
            this.userText.CustomButton.Location = new System.Drawing.Point(298, 1);
            this.userText.CustomButton.Name = "";
            this.userText.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.userText.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.userText.CustomButton.TabIndex = 1;
            this.userText.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.userText.CustomButton.UseSelectable = true;
            this.userText.CustomButton.Visible = false;
            this.userText.Lines = new string[0];
            this.userText.Location = new System.Drawing.Point(23, 86);
            this.userText.MaxLength = 32767;
            this.userText.Name = "userText";
            this.userText.PasswordChar = '\0';
            this.userText.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.userText.SelectedText = "";
            this.userText.SelectionLength = 0;
            this.userText.SelectionStart = 0;
            this.userText.Size = new System.Drawing.Size(320, 23);
            this.userText.Style = MetroFramework.MetroColorStyle.Brown;
            this.userText.TabIndex = 0;
            this.userText.UseSelectable = true;
            this.userText.WaterMark = "Username";
            this.userText.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.userText.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // passText
            // 
            this.passText.Anchor = System.Windows.Forms.AnchorStyles.None;
            // 
            // 
            // 
            this.passText.CustomButton.Image = null;
            this.passText.CustomButton.Location = new System.Drawing.Point(298, 1);
            this.passText.CustomButton.Name = "";
            this.passText.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.passText.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.passText.CustomButton.TabIndex = 1;
            this.passText.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.passText.CustomButton.UseSelectable = true;
            this.passText.CustomButton.Visible = false;
            this.passText.Lines = new string[0];
            this.passText.Location = new System.Drawing.Point(23, 115);
            this.passText.MaxLength = 32767;
            this.passText.Name = "passText";
            this.passText.PasswordChar = '\0';
            this.passText.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.passText.SelectedText = "";
            this.passText.SelectionLength = 0;
            this.passText.SelectionStart = 0;
            this.passText.Size = new System.Drawing.Size(320, 23);
            this.passText.Style = MetroFramework.MetroColorStyle.Brown;
            this.passText.TabIndex = 1;
            this.passText.UseSelectable = true;
            this.passText.WaterMark = "Password";
            this.passText.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.passText.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // loginButton
            // 
            this.loginButton.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.loginButton.Image = null;
            this.loginButton.Location = new System.Drawing.Point(230, 197);
            this.loginButton.Name = "loginButton";
            this.loginButton.Size = new System.Drawing.Size(113, 40);
            this.loginButton.TabIndex = 2;
            this.loginButton.Text = "Login";
            this.loginButton.UseSelectable = true;
            this.loginButton.UseVisualStyleBackColor = true;
            this.loginButton.Click += new System.EventHandler(this.loginButton_Click);
            // 
            // exitButton
            // 
            this.exitButton.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.exitButton.Location = new System.Drawing.Point(23, 197);
            this.exitButton.Name = "exitButton";
            this.exitButton.Size = new System.Drawing.Size(113, 40);
            this.exitButton.TabIndex = 3;
            this.exitButton.Text = "Exit";
            this.exitButton.UseSelectable = true;
            this.exitButton.Click += new System.EventHandler(this.exitButton_Click);
            // 
            // loginForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.BackMaxSize = 40;
            this.ClientSize = new System.Drawing.Size(366, 260);
            this.ControlBox = false;
            this.Controls.Add(this.exitButton);
            this.Controls.Add(this.loginButton);
            this.Controls.Add(this.passText);
            this.Controls.Add(this.userText);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "loginForm";
            this.Style = MetroFramework.MetroColorStyle.Black;
            this.Text = "Login";
            this.Load += new System.EventHandler(this.loginForm_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private MetroFramework.Controls.MetroTextBox userText;
        private MetroFramework.Controls.MetroTextBox passText;
        private MetroFramework.Controls.MetroTextBox.MetroTextButton loginButton;
        private MetroFramework.Controls.MetroButton exitButton;
    }
}