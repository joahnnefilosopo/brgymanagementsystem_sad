﻿namespace panelTransition.Panel
{
    partial class secMem
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(secMem));
            this.metroTabControl1 = new MetroFramework.Controls.MetroTabControl();
            this.metroTabPage1 = new MetroFramework.Controls.MetroTabPage();
            this.genderCBox = new MetroFramework.Controls.MetroComboBox();
            this.metroLabel22 = new MetroFramework.Controls.MetroLabel();
            this.addProfileButton = new MetroFramework.Controls.MetroTextBox.MetroTextButton();
            this.purokText = new MetroFramework.Controls.MetroTextBox();
            this.label1 = new MetroFramework.Controls.MetroLabel();
            this.empCBox = new MetroFramework.Controls.MetroComboBox();
            this.incomeCBox = new MetroFramework.Controls.MetroComboBox();
            this.busscontactText = new MetroFramework.Controls.MetroTextBox();
            this.busaddrichTextBox = new MetroFramework.Controls.MetroTextBox();
            this.occupText = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel21 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel20 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel19 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel18 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel17 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel16 = new MetroFramework.Controls.MetroLabel();
            this.htmlPanel1 = new MetroFramework.Drawing.Html.HtmlPanel();
            this.housetypeText = new MetroFramework.Controls.MetroTextBox();
            this.educattainText = new MetroFramework.Controls.MetroComboBox();
            this.emaddText = new MetroFramework.Controls.MetroTextBox();
            this.mobileText = new MetroFramework.Controls.MetroTextBox();
            this.telnoText = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel15 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel14 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel13 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel12 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel11 = new MetroFramework.Controls.MetroLabel();
            this.restypeText = new MetroFramework.Controls.MetroComboBox();
            this.metroLabel10 = new MetroFramework.Controls.MetroLabel();
            this.startdatePicker = new MetroFramework.Controls.MetroDateTime();
            this.metroLabel9 = new MetroFramework.Controls.MetroLabel();
            this.addressRichTextBox = new MetroFramework.Controls.MetroTextBox();
            this.relText = new MetroFramework.Controls.MetroTextBox();
            this.civilstatusCBox = new MetroFramework.Controls.MetroComboBox();
            this.bdatePicker = new MetroFramework.Controls.MetroDateTime();
            this.lnameText = new MetroFramework.Controls.MetroTextBox();
            this.mnameText = new MetroFramework.Controls.MetroTextBox();
            this.fnameText = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel8 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel7 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel6 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel5 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel4 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel3 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel2 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel1 = new MetroFramework.Controls.MetroLabel();
            this.metroTabPage2 = new MetroFramework.Controls.MetroTabPage();
            this.metroGrid1 = new MetroFramework.Controls.MetroGrid();
            this.name = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.year = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.course = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.metroTextBox1 = new MetroFramework.Controls.MetroTextBox();
            this.metroTabPage3 = new MetroFramework.Controls.MetroTabPage();
            this.metroPanel1 = new MetroFramework.Controls.MetroPanel();
            this.viewInfoButton = new MetroFramework.Controls.MetroButton();
            this.metroTabControl1.SuspendLayout();
            this.metroTabPage1.SuspendLayout();
            this.metroTabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.metroGrid1)).BeginInit();
            this.SuspendLayout();
            // 
            // metroTabControl1
            // 
            this.metroTabControl1.Controls.Add(this.metroTabPage1);
            this.metroTabControl1.Controls.Add(this.metroTabPage2);
            this.metroTabControl1.Controls.Add(this.metroTabPage3);
            this.metroTabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.metroTabControl1.Location = new System.Drawing.Point(0, 0);
            this.metroTabControl1.Name = "metroTabControl1";
            this.metroTabControl1.SelectedIndex = 1;
            this.metroTabControl1.Size = new System.Drawing.Size(993, 734);
            this.metroTabControl1.Style = MetroFramework.MetroColorStyle.Brown;
            this.metroTabControl1.TabIndex = 0;
            this.metroTabControl1.UseSelectable = true;
            // 
            // metroTabPage1
            // 
            this.metroTabPage1.AutoScroll = true;
            this.metroTabPage1.BackColor = System.Drawing.Color.White;
            this.metroTabPage1.Controls.Add(this.genderCBox);
            this.metroTabPage1.Controls.Add(this.metroLabel22);
            this.metroTabPage1.Controls.Add(this.addProfileButton);
            this.metroTabPage1.Controls.Add(this.purokText);
            this.metroTabPage1.Controls.Add(this.label1);
            this.metroTabPage1.Controls.Add(this.empCBox);
            this.metroTabPage1.Controls.Add(this.incomeCBox);
            this.metroTabPage1.Controls.Add(this.busscontactText);
            this.metroTabPage1.Controls.Add(this.busaddrichTextBox);
            this.metroTabPage1.Controls.Add(this.occupText);
            this.metroTabPage1.Controls.Add(this.metroLabel21);
            this.metroTabPage1.Controls.Add(this.metroLabel20);
            this.metroTabPage1.Controls.Add(this.metroLabel19);
            this.metroTabPage1.Controls.Add(this.metroLabel18);
            this.metroTabPage1.Controls.Add(this.metroLabel17);
            this.metroTabPage1.Controls.Add(this.metroLabel16);
            this.metroTabPage1.Controls.Add(this.htmlPanel1);
            this.metroTabPage1.Controls.Add(this.housetypeText);
            this.metroTabPage1.Controls.Add(this.educattainText);
            this.metroTabPage1.Controls.Add(this.emaddText);
            this.metroTabPage1.Controls.Add(this.mobileText);
            this.metroTabPage1.Controls.Add(this.telnoText);
            this.metroTabPage1.Controls.Add(this.metroLabel15);
            this.metroTabPage1.Controls.Add(this.metroLabel14);
            this.metroTabPage1.Controls.Add(this.metroLabel13);
            this.metroTabPage1.Controls.Add(this.metroLabel12);
            this.metroTabPage1.Controls.Add(this.metroLabel11);
            this.metroTabPage1.Controls.Add(this.restypeText);
            this.metroTabPage1.Controls.Add(this.metroLabel10);
            this.metroTabPage1.Controls.Add(this.startdatePicker);
            this.metroTabPage1.Controls.Add(this.metroLabel9);
            this.metroTabPage1.Controls.Add(this.addressRichTextBox);
            this.metroTabPage1.Controls.Add(this.relText);
            this.metroTabPage1.Controls.Add(this.civilstatusCBox);
            this.metroTabPage1.Controls.Add(this.bdatePicker);
            this.metroTabPage1.Controls.Add(this.lnameText);
            this.metroTabPage1.Controls.Add(this.mnameText);
            this.metroTabPage1.Controls.Add(this.fnameText);
            this.metroTabPage1.Controls.Add(this.metroLabel8);
            this.metroTabPage1.Controls.Add(this.metroLabel7);
            this.metroTabPage1.Controls.Add(this.metroLabel6);
            this.metroTabPage1.Controls.Add(this.metroLabel5);
            this.metroTabPage1.Controls.Add(this.metroLabel4);
            this.metroTabPage1.Controls.Add(this.metroLabel3);
            this.metroTabPage1.Controls.Add(this.metroLabel2);
            this.metroTabPage1.Controls.Add(this.metroLabel1);
            this.metroTabPage1.HorizontalScrollbar = true;
            this.metroTabPage1.HorizontalScrollbarBarColor = false;
            this.metroTabPage1.HorizontalScrollbarHighlightOnWheel = true;
            this.metroTabPage1.HorizontalScrollbarSize = 10;
            this.metroTabPage1.Location = new System.Drawing.Point(4, 38);
            this.metroTabPage1.Name = "metroTabPage1";
            this.metroTabPage1.Size = new System.Drawing.Size(985, 692);
            this.metroTabPage1.Style = MetroFramework.MetroColorStyle.Brown;
            this.metroTabPage1.TabIndex = 0;
            this.metroTabPage1.Text = "ADD MEMBER PROFILE";
            this.metroTabPage1.Theme = MetroFramework.MetroThemeStyle.Light;
            this.metroTabPage1.UseCustomBackColor = true;
            this.metroTabPage1.VerticalScrollbar = true;
            this.metroTabPage1.VerticalScrollbarBarColor = true;
            this.metroTabPage1.VerticalScrollbarHighlightOnWheel = false;
            this.metroTabPage1.VerticalScrollbarSize = 10;
            this.metroTabPage1.Click += new System.EventHandler(this.metroTabPage1_Click);
            // 
            // genderCBox
            // 
            this.genderCBox.FormattingEnabled = true;
            this.genderCBox.ItemHeight = 23;
            this.genderCBox.Items.AddRange(new object[] {
            "Male",
            "Female"});
            this.genderCBox.Location = new System.Drawing.Point(39, 145);
            this.genderCBox.Name = "genderCBox";
            this.genderCBox.Size = new System.Drawing.Size(121, 29);
            this.genderCBox.Style = MetroFramework.MetroColorStyle.Brown;
            this.genderCBox.TabIndex = 5;
            this.genderCBox.UseSelectable = true;
            this.genderCBox.SelectedIndexChanged += new System.EventHandler(this.metroComboBox1_SelectedIndexChanged);
            // 
            // metroLabel22
            // 
            this.metroLabel22.AutoSize = true;
            this.metroLabel22.Location = new System.Drawing.Point(39, 123);
            this.metroLabel22.Name = "metroLabel22";
            this.metroLabel22.Size = new System.Drawing.Size(52, 19);
            this.metroLabel22.Style = MetroFramework.MetroColorStyle.Brown;
            this.metroLabel22.TabIndex = 45;
            this.metroLabel22.Text = "Gender";
            this.metroLabel22.Click += new System.EventHandler(this.metroLabel22_Click);
            // 
            // addProfileButton
            // 
            this.addProfileButton.Image = null;
            this.addProfileButton.Location = new System.Drawing.Point(751, 619);
            this.addProfileButton.Name = "addProfileButton";
            this.addProfileButton.Size = new System.Drawing.Size(110, 43);
            this.addProfileButton.Style = MetroFramework.MetroColorStyle.Brown;
            this.addProfileButton.TabIndex = 22;
            this.addProfileButton.Text = "Add Member";
            this.addProfileButton.UseSelectable = true;
            this.addProfileButton.UseStyleColors = true;
            this.addProfileButton.UseVisualStyleBackColor = true;
            this.addProfileButton.Click += new System.EventHandler(this.addProfileButton_Click);
            // 
            // purokText
            // 
            // 
            // 
            // 
            this.purokText.CustomButton.Image = null;
            this.purokText.CustomButton.Location = new System.Drawing.Point(97, 1);
            this.purokText.CustomButton.Name = "";
            this.purokText.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.purokText.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.purokText.CustomButton.TabIndex = 1;
            this.purokText.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.purokText.CustomButton.UseSelectable = true;
            this.purokText.CustomButton.Visible = false;
            this.purokText.Lines = new string[0];
            this.purokText.Location = new System.Drawing.Point(39, 74);
            this.purokText.MaxLength = 32767;
            this.purokText.Name = "purokText";
            this.purokText.PasswordChar = '\0';
            this.purokText.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.purokText.SelectedText = "";
            this.purokText.SelectionLength = 0;
            this.purokText.SelectionStart = 0;
            this.purokText.Size = new System.Drawing.Size(119, 23);
            this.purokText.TabIndex = 1;
            this.purokText.UseSelectable = true;
            this.purokText.WaterMark = "Enter purok";
            this.purokText.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.purokText.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(39, 52);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(43, 19);
            this.label1.TabIndex = 43;
            this.label1.Text = "Purok";
            // 
            // empCBox
            // 
            this.empCBox.FormattingEnabled = true;
            this.empCBox.ItemHeight = 23;
            this.empCBox.Items.AddRange(new object[] {
            "Employed",
            "Unemployed",
            "Self-employed"});
            this.empCBox.Location = new System.Drawing.Point(408, 514);
            this.empCBox.Name = "empCBox";
            this.empCBox.Size = new System.Drawing.Size(241, 29);
            this.empCBox.Style = MetroFramework.MetroColorStyle.Brown;
            this.empCBox.TabIndex = 21;
            this.empCBox.UseSelectable = true;
            // 
            // incomeCBox
            // 
            this.incomeCBox.FormattingEnabled = true;
            this.incomeCBox.ItemHeight = 23;
            this.incomeCBox.Items.AddRange(new object[] {
            "Less than 3,000",
            "5,000 - 10,000",
            "10,001 - 15,000",
            "15,001 - 20,000",
            "20,001 - 30,001",
            "Over 30,000"});
            this.incomeCBox.Location = new System.Drawing.Point(506, 453);
            this.incomeCBox.Name = "incomeCBox";
            this.incomeCBox.Size = new System.Drawing.Size(207, 29);
            this.incomeCBox.Style = MetroFramework.MetroColorStyle.Brown;
            this.incomeCBox.TabIndex = 19;
            this.incomeCBox.UseSelectable = true;
            // 
            // busscontactText
            // 
            // 
            // 
            // 
            this.busscontactText.CustomButton.Image = null;
            this.busscontactText.CustomButton.Location = new System.Drawing.Point(181, 1);
            this.busscontactText.CustomButton.Name = "";
            this.busscontactText.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.busscontactText.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.busscontactText.CustomButton.TabIndex = 1;
            this.busscontactText.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.busscontactText.CustomButton.UseSelectable = true;
            this.busscontactText.CustomButton.Visible = false;
            this.busscontactText.Lines = new string[0];
            this.busscontactText.Location = new System.Drawing.Point(272, 452);
            this.busscontactText.MaxLength = 32767;
            this.busscontactText.Name = "busscontactText";
            this.busscontactText.PasswordChar = '\0';
            this.busscontactText.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.busscontactText.SelectedText = "";
            this.busscontactText.SelectionLength = 0;
            this.busscontactText.SelectionStart = 0;
            this.busscontactText.Size = new System.Drawing.Size(203, 23);
            this.busscontactText.Style = MetroFramework.MetroColorStyle.Brown;
            this.busscontactText.TabIndex = 18;
            this.busscontactText.UseSelectable = true;
            this.busscontactText.WaterMark = "xxx-xxxxx";
            this.busscontactText.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.busscontactText.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // busaddrichTextBox
            // 
            // 
            // 
            // 
            this.busaddrichTextBox.CustomButton.Image = null;
            this.busaddrichTextBox.CustomButton.Location = new System.Drawing.Point(296, 2);
            this.busaddrichTextBox.CustomButton.Name = "";
            this.busaddrichTextBox.CustomButton.Size = new System.Drawing.Size(53, 53);
            this.busaddrichTextBox.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.busaddrichTextBox.CustomButton.TabIndex = 1;
            this.busaddrichTextBox.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.busaddrichTextBox.CustomButton.UseSelectable = true;
            this.busaddrichTextBox.CustomButton.Visible = false;
            this.busaddrichTextBox.Lines = new string[0];
            this.busaddrichTextBox.Location = new System.Drawing.Point(39, 514);
            this.busaddrichTextBox.MaxLength = 32767;
            this.busaddrichTextBox.Multiline = true;
            this.busaddrichTextBox.Name = "busaddrichTextBox";
            this.busaddrichTextBox.PasswordChar = '\0';
            this.busaddrichTextBox.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.busaddrichTextBox.SelectedText = "";
            this.busaddrichTextBox.SelectionLength = 0;
            this.busaddrichTextBox.SelectionStart = 0;
            this.busaddrichTextBox.Size = new System.Drawing.Size(352, 58);
            this.busaddrichTextBox.Style = MetroFramework.MetroColorStyle.Brown;
            this.busaddrichTextBox.TabIndex = 20;
            this.busaddrichTextBox.UseSelectable = true;
            this.busaddrichTextBox.WaterMark = "Enter your address";
            this.busaddrichTextBox.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.busaddrichTextBox.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // occupText
            // 
            // 
            // 
            // 
            this.occupText.CustomButton.Image = null;
            this.occupText.CustomButton.Location = new System.Drawing.Point(185, 1);
            this.occupText.CustomButton.Name = "";
            this.occupText.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.occupText.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.occupText.CustomButton.TabIndex = 1;
            this.occupText.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.occupText.CustomButton.UseSelectable = true;
            this.occupText.CustomButton.Visible = false;
            this.occupText.Lines = new string[0];
            this.occupText.Location = new System.Drawing.Point(39, 453);
            this.occupText.MaxLength = 32767;
            this.occupText.Name = "occupText";
            this.occupText.PasswordChar = '\0';
            this.occupText.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.occupText.SelectedText = "";
            this.occupText.SelectionLength = 0;
            this.occupText.SelectionStart = 0;
            this.occupText.Size = new System.Drawing.Size(207, 23);
            this.occupText.Style = MetroFramework.MetroColorStyle.Brown;
            this.occupText.TabIndex = 17;
            this.occupText.UseSelectable = true;
            this.occupText.WaterMark = "Enter your occupation";
            this.occupText.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.occupText.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroLabel21
            // 
            this.metroLabel21.AutoSize = true;
            this.metroLabel21.Location = new System.Drawing.Point(39, 492);
            this.metroLabel21.Name = "metroLabel21";
            this.metroLabel21.Size = new System.Drawing.Size(107, 19);
            this.metroLabel21.Style = MetroFramework.MetroColorStyle.Brown;
            this.metroLabel21.TabIndex = 37;
            this.metroLabel21.Text = "Business Address";
            // 
            // metroLabel20
            // 
            this.metroLabel20.AutoSize = true;
            this.metroLabel20.Location = new System.Drawing.Point(39, 431);
            this.metroLabel20.Name = "metroLabel20";
            this.metroLabel20.Size = new System.Drawing.Size(76, 19);
            this.metroLabel20.Style = MetroFramework.MetroColorStyle.Brown;
            this.metroLabel20.TabIndex = 36;
            this.metroLabel20.Text = "Occupation";
            // 
            // metroLabel19
            // 
            this.metroLabel19.AutoSize = true;
            this.metroLabel19.Location = new System.Drawing.Point(408, 492);
            this.metroLabel19.Name = "metroLabel19";
            this.metroLabel19.Size = new System.Drawing.Size(83, 19);
            this.metroLabel19.Style = MetroFramework.MetroColorStyle.Brown;
            this.metroLabel19.TabIndex = 35;
            this.metroLabel19.Text = "Employment";
            // 
            // metroLabel18
            // 
            this.metroLabel18.AutoSize = true;
            this.metroLabel18.Location = new System.Drawing.Point(506, 431);
            this.metroLabel18.Name = "metroLabel18";
            this.metroLabel18.Size = new System.Drawing.Size(96, 19);
            this.metroLabel18.Style = MetroFramework.MetroColorStyle.Brown;
            this.metroLabel18.TabIndex = 34;
            this.metroLabel18.Text = "Income Branch";
            // 
            // metroLabel17
            // 
            this.metroLabel17.AutoSize = true;
            this.metroLabel17.Location = new System.Drawing.Point(272, 431);
            this.metroLabel17.Name = "metroLabel17";
            this.metroLabel17.Size = new System.Drawing.Size(130, 19);
            this.metroLabel17.Style = MetroFramework.MetroColorStyle.Brown;
            this.metroLabel17.TabIndex = 33;
            this.metroLabel17.Text = "Business Contact No.";
            // 
            // metroLabel16
            // 
            this.metroLabel16.AutoSize = true;
            this.metroLabel16.FontWeight = MetroFramework.MetroLabelWeight.Regular;
            this.metroLabel16.Location = new System.Drawing.Point(39, 396);
            this.metroLabel16.Name = "metroLabel16";
            this.metroLabel16.Size = new System.Drawing.Size(118, 19);
            this.metroLabel16.Style = MetroFramework.MetroColorStyle.Brown;
            this.metroLabel16.TabIndex = 32;
            this.metroLabel16.Text = "Work Information";
            this.metroLabel16.UseStyleColors = true;
            // 
            // htmlPanel1
            // 
            this.htmlPanel1.AutoScroll = true;
            this.htmlPanel1.AutoScrollMinSize = new System.Drawing.Size(822, 2);
            this.htmlPanel1.BackColor = System.Drawing.SystemColors.Window;
            this.htmlPanel1.Location = new System.Drawing.Point(39, 371);
            this.htmlPanel1.Name = "htmlPanel1";
            this.htmlPanel1.Size = new System.Drawing.Size(822, 22);
            this.htmlPanel1.TabIndex = 31;
            this.htmlPanel1.Text = "<hr>";
            // 
            // housetypeText
            // 
            // 
            // 
            // 
            this.housetypeText.CustomButton.Image = null;
            this.housetypeText.CustomButton.Location = new System.Drawing.Point(157, 1);
            this.housetypeText.CustomButton.Name = "";
            this.housetypeText.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.housetypeText.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.housetypeText.CustomButton.TabIndex = 1;
            this.housetypeText.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.housetypeText.CustomButton.UseSelectable = true;
            this.housetypeText.CustomButton.Visible = false;
            this.housetypeText.Lines = new string[0];
            this.housetypeText.Location = new System.Drawing.Point(779, 213);
            this.housetypeText.MaxLength = 32767;
            this.housetypeText.Name = "housetypeText";
            this.housetypeText.PasswordChar = '\0';
            this.housetypeText.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.housetypeText.SelectedText = "";
            this.housetypeText.SelectionLength = 0;
            this.housetypeText.SelectionStart = 0;
            this.housetypeText.Size = new System.Drawing.Size(179, 23);
            this.housetypeText.Style = MetroFramework.MetroColorStyle.Brown;
            this.housetypeText.TabIndex = 13;
            this.housetypeText.UseSelectable = true;
            this.housetypeText.WaterMark = "Enter your house type";
            this.housetypeText.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.housetypeText.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // educattainText
            // 
            this.educattainText.FormattingEnabled = true;
            this.educattainText.ItemHeight = 23;
            this.educattainText.Items.AddRange(new object[] {
            "Pre-school",
            "Gradeschool",
            "Highschool",
            "College",
            "Masteral",
            "Doctorate",
            "None"});
            this.educattainText.Location = new System.Drawing.Point(441, 145);
            this.educattainText.Name = "educattainText";
            this.educattainText.Size = new System.Drawing.Size(227, 29);
            this.educattainText.TabIndex = 8;
            this.educattainText.UseSelectable = true;
            // 
            // emaddText
            // 
            // 
            // 
            // 
            this.emaddText.CustomButton.Image = null;
            this.emaddText.CustomButton.Location = new System.Drawing.Point(185, 1);
            this.emaddText.CustomButton.Name = "";
            this.emaddText.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.emaddText.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.emaddText.CustomButton.TabIndex = 1;
            this.emaddText.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.emaddText.CustomButton.UseSelectable = true;
            this.emaddText.CustomButton.Visible = false;
            this.emaddText.Lines = new string[0];
            this.emaddText.Location = new System.Drawing.Point(506, 318);
            this.emaddText.MaxLength = 32767;
            this.emaddText.Name = "emaddText";
            this.emaddText.PasswordChar = '\0';
            this.emaddText.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.emaddText.SelectedText = "";
            this.emaddText.SelectionLength = 0;
            this.emaddText.SelectionStart = 0;
            this.emaddText.Size = new System.Drawing.Size(207, 23);
            this.emaddText.Style = MetroFramework.MetroColorStyle.Brown;
            this.emaddText.TabIndex = 16;
            this.emaddText.UseSelectable = true;
            this.emaddText.WaterMark = "Enter your email address";
            this.emaddText.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.emaddText.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // mobileText
            // 
            // 
            // 
            // 
            this.mobileText.CustomButton.Image = null;
            this.mobileText.CustomButton.Location = new System.Drawing.Point(154, 1);
            this.mobileText.CustomButton.Name = "";
            this.mobileText.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.mobileText.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.mobileText.CustomButton.TabIndex = 1;
            this.mobileText.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.mobileText.CustomButton.UseSelectable = true;
            this.mobileText.CustomButton.Visible = false;
            this.mobileText.Lines = new string[0];
            this.mobileText.Location = new System.Drawing.Point(272, 318);
            this.mobileText.MaxLength = 32767;
            this.mobileText.Name = "mobileText";
            this.mobileText.PasswordChar = '\0';
            this.mobileText.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.mobileText.SelectedText = "";
            this.mobileText.SelectionLength = 0;
            this.mobileText.SelectionStart = 0;
            this.mobileText.Size = new System.Drawing.Size(176, 23);
            this.mobileText.Style = MetroFramework.MetroColorStyle.Brown;
            this.mobileText.TabIndex = 15;
            this.mobileText.UseSelectable = true;
            this.mobileText.WaterMark = "09xxxxxxxxx";
            this.mobileText.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.mobileText.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // telnoText
            // 
            // 
            // 
            // 
            this.telnoText.CustomButton.Image = null;
            this.telnoText.CustomButton.Location = new System.Drawing.Point(154, 1);
            this.telnoText.CustomButton.Name = "";
            this.telnoText.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.telnoText.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.telnoText.CustomButton.TabIndex = 1;
            this.telnoText.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.telnoText.CustomButton.UseSelectable = true;
            this.telnoText.CustomButton.Visible = false;
            this.telnoText.Lines = new string[0];
            this.telnoText.Location = new System.Drawing.Point(39, 318);
            this.telnoText.MaxLength = 32767;
            this.telnoText.Name = "telnoText";
            this.telnoText.PasswordChar = '\0';
            this.telnoText.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.telnoText.SelectedText = "";
            this.telnoText.SelectionLength = 0;
            this.telnoText.SelectionStart = 0;
            this.telnoText.Size = new System.Drawing.Size(176, 23);
            this.telnoText.Style = MetroFramework.MetroColorStyle.Brown;
            this.telnoText.TabIndex = 14;
            this.telnoText.UseSelectable = true;
            this.telnoText.WaterMark = "xxx-xxxx";
            this.telnoText.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.telnoText.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroLabel15
            // 
            this.metroLabel15.AutoSize = true;
            this.metroLabel15.Location = new System.Drawing.Point(441, 123);
            this.metroLabel15.Name = "metroLabel15";
            this.metroLabel15.Size = new System.Drawing.Size(191, 19);
            this.metroLabel15.TabIndex = 25;
            this.metroLabel15.Text = "Highest Educational Attainment";
            // 
            // metroLabel14
            // 
            this.metroLabel14.AutoSize = true;
            this.metroLabel14.Location = new System.Drawing.Point(506, 296);
            this.metroLabel14.Name = "metroLabel14";
            this.metroLabel14.Size = new System.Drawing.Size(92, 19);
            this.metroLabel14.TabIndex = 24;
            this.metroLabel14.Text = "Email Address";
            // 
            // metroLabel13
            // 
            this.metroLabel13.AutoSize = true;
            this.metroLabel13.Location = new System.Drawing.Point(779, 191);
            this.metroLabel13.Name = "metroLabel13";
            this.metroLabel13.Size = new System.Drawing.Size(76, 19);
            this.metroLabel13.TabIndex = 23;
            this.metroLabel13.Text = "House Type";
            // 
            // metroLabel12
            // 
            this.metroLabel12.AutoSize = true;
            this.metroLabel12.Location = new System.Drawing.Point(272, 296);
            this.metroLabel12.Name = "metroLabel12";
            this.metroLabel12.Size = new System.Drawing.Size(97, 19);
            this.metroLabel12.TabIndex = 22;
            this.metroLabel12.Text = "Mobile Tel. No.";
            // 
            // metroLabel11
            // 
            this.metroLabel11.AutoSize = true;
            this.metroLabel11.Location = new System.Drawing.Point(39, 296);
            this.metroLabel11.Name = "metroLabel11";
            this.metroLabel11.Size = new System.Drawing.Size(92, 19);
            this.metroLabel11.TabIndex = 21;
            this.metroLabel11.Text = "Home Tel. No.";
            // 
            // restypeText
            // 
            this.restypeText.FormattingEnabled = true;
            this.restypeText.ItemHeight = 23;
            this.restypeText.Items.AddRange(new object[] {
            "Owner",
            "Renter",
            "Boarder",
            "Relative of Owner"});
            this.restypeText.Location = new System.Drawing.Point(607, 213);
            this.restypeText.Name = "restypeText";
            this.restypeText.Size = new System.Drawing.Size(153, 29);
            this.restypeText.Style = MetroFramework.MetroColorStyle.Brown;
            this.restypeText.TabIndex = 12;
            this.restypeText.UseSelectable = true;
            // 
            // metroLabel10
            // 
            this.metroLabel10.AutoSize = true;
            this.metroLabel10.Location = new System.Drawing.Point(607, 191);
            this.metroLabel10.Name = "metroLabel10";
            this.metroLabel10.Size = new System.Drawing.Size(114, 19);
            this.metroLabel10.TabIndex = 19;
            this.metroLabel10.Text = "Type of Residence";
            // 
            // startdatePicker
            // 
            this.startdatePicker.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.startdatePicker.Location = new System.Drawing.Point(405, 213);
            this.startdatePicker.MinimumSize = new System.Drawing.Size(0, 29);
            this.startdatePicker.Name = "startdatePicker";
            this.startdatePicker.Size = new System.Drawing.Size(119, 29);
            this.startdatePicker.Style = MetroFramework.MetroColorStyle.Brown;
            this.startdatePicker.TabIndex = 11;
            // 
            // metroLabel9
            // 
            this.metroLabel9.AutoSize = true;
            this.metroLabel9.Location = new System.Drawing.Point(405, 191);
            this.metroLabel9.Name = "metroLabel9";
            this.metroLabel9.Size = new System.Drawing.Size(193, 19);
            this.metroLabel9.TabIndex = 17;
            this.metroLabel9.Text = "Date Started Living in Barangay";
            // 
            // addressRichTextBox
            // 
            // 
            // 
            // 
            this.addressRichTextBox.CustomButton.Image = null;
            this.addressRichTextBox.CustomButton.Location = new System.Drawing.Point(296, 2);
            this.addressRichTextBox.CustomButton.Name = "";
            this.addressRichTextBox.CustomButton.Size = new System.Drawing.Size(53, 53);
            this.addressRichTextBox.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.addressRichTextBox.CustomButton.TabIndex = 1;
            this.addressRichTextBox.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.addressRichTextBox.CustomButton.UseSelectable = true;
            this.addressRichTextBox.CustomButton.Visible = false;
            this.addressRichTextBox.Lines = new string[0];
            this.addressRichTextBox.Location = new System.Drawing.Point(39, 213);
            this.addressRichTextBox.MaxLength = 32767;
            this.addressRichTextBox.Multiline = true;
            this.addressRichTextBox.Name = "addressRichTextBox";
            this.addressRichTextBox.PasswordChar = '\0';
            this.addressRichTextBox.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.addressRichTextBox.SelectedText = "";
            this.addressRichTextBox.SelectionLength = 0;
            this.addressRichTextBox.SelectionStart = 0;
            this.addressRichTextBox.Size = new System.Drawing.Size(352, 58);
            this.addressRichTextBox.Style = MetroFramework.MetroColorStyle.Brown;
            this.addressRichTextBox.TabIndex = 10;
            this.addressRichTextBox.UseSelectable = true;
            this.addressRichTextBox.WaterMark = "Enter your address";
            this.addressRichTextBox.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.addressRichTextBox.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // relText
            // 
            // 
            // 
            // 
            this.relText.CustomButton.Image = null;
            this.relText.CustomButton.Location = new System.Drawing.Point(196, 1);
            this.relText.CustomButton.Name = "";
            this.relText.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.relText.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.relText.CustomButton.TabIndex = 1;
            this.relText.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.relText.CustomButton.UseSelectable = true;
            this.relText.CustomButton.Visible = false;
            this.relText.Lines = new string[0];
            this.relText.Location = new System.Drawing.Point(698, 145);
            this.relText.MaxLength = 32767;
            this.relText.Name = "relText";
            this.relText.PasswordChar = '\0';
            this.relText.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.relText.SelectedText = "";
            this.relText.SelectionLength = 0;
            this.relText.SelectionStart = 0;
            this.relText.Size = new System.Drawing.Size(218, 23);
            this.relText.Style = MetroFramework.MetroColorStyle.Brown;
            this.relText.TabIndex = 9;
            this.relText.UseSelectable = true;
            this.relText.WaterMark = "Enter your religion";
            this.relText.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.relText.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // civilstatusCBox
            // 
            this.civilstatusCBox.FormattingEnabled = true;
            this.civilstatusCBox.ItemHeight = 23;
            this.civilstatusCBox.Items.AddRange(new object[] {
            "Single",
            "Married",
            "Separated",
            "Window"});
            this.civilstatusCBox.Location = new System.Drawing.Point(309, 145);
            this.civilstatusCBox.Name = "civilstatusCBox";
            this.civilstatusCBox.Size = new System.Drawing.Size(121, 29);
            this.civilstatusCBox.Style = MetroFramework.MetroColorStyle.Brown;
            this.civilstatusCBox.TabIndex = 7;
            this.civilstatusCBox.UseSelectable = true;
            // 
            // bdatePicker
            // 
            this.bdatePicker.FontWeight = MetroFramework.MetroDateTimeWeight.Light;
            this.bdatePicker.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.bdatePicker.Location = new System.Drawing.Point(173, 145);
            this.bdatePicker.MinimumSize = new System.Drawing.Size(0, 29);
            this.bdatePicker.Name = "bdatePicker";
            this.bdatePicker.Size = new System.Drawing.Size(119, 29);
            this.bdatePicker.Style = MetroFramework.MetroColorStyle.Brown;
            this.bdatePicker.TabIndex = 6;
            // 
            // lnameText
            // 
            // 
            // 
            // 
            this.lnameText.CustomButton.Image = null;
            this.lnameText.CustomButton.Location = new System.Drawing.Point(208, 1);
            this.lnameText.CustomButton.Name = "";
            this.lnameText.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.lnameText.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.lnameText.CustomButton.TabIndex = 1;
            this.lnameText.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.lnameText.CustomButton.UseSelectable = true;
            this.lnameText.CustomButton.Visible = false;
            this.lnameText.Lines = new string[0];
            this.lnameText.Location = new System.Drawing.Point(631, 74);
            this.lnameText.MaxLength = 32767;
            this.lnameText.Name = "lnameText";
            this.lnameText.PasswordChar = '\0';
            this.lnameText.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.lnameText.SelectedText = "";
            this.lnameText.SelectionLength = 0;
            this.lnameText.SelectionStart = 0;
            this.lnameText.Size = new System.Drawing.Size(230, 23);
            this.lnameText.Style = MetroFramework.MetroColorStyle.Brown;
            this.lnameText.TabIndex = 4;
            this.lnameText.UseSelectable = true;
            this.lnameText.WaterMark = "Enter your last name";
            this.lnameText.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.lnameText.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // mnameText
            // 
            // 
            // 
            // 
            this.mnameText.CustomButton.Image = null;
            this.mnameText.CustomButton.Location = new System.Drawing.Point(185, 1);
            this.mnameText.CustomButton.Name = "";
            this.mnameText.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.mnameText.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.mnameText.CustomButton.TabIndex = 1;
            this.mnameText.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.mnameText.CustomButton.UseSelectable = true;
            this.mnameText.CustomButton.Visible = false;
            this.mnameText.Lines = new string[0];
            this.mnameText.Location = new System.Drawing.Point(397, 74);
            this.mnameText.MaxLength = 32767;
            this.mnameText.Name = "mnameText";
            this.mnameText.PasswordChar = '\0';
            this.mnameText.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.mnameText.SelectedText = "";
            this.mnameText.SelectionLength = 0;
            this.mnameText.SelectionStart = 0;
            this.mnameText.Size = new System.Drawing.Size(207, 23);
            this.mnameText.Style = MetroFramework.MetroColorStyle.Brown;
            this.mnameText.TabIndex = 3;
            this.mnameText.UseSelectable = true;
            this.mnameText.WaterMark = "Enter your middle name";
            this.mnameText.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.mnameText.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // fnameText
            // 
            // 
            // 
            // 
            this.fnameText.CustomButton.Image = null;
            this.fnameText.CustomButton.Location = new System.Drawing.Point(196, 1);
            this.fnameText.CustomButton.Name = "";
            this.fnameText.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.fnameText.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.fnameText.CustomButton.TabIndex = 1;
            this.fnameText.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.fnameText.CustomButton.UseSelectable = true;
            this.fnameText.CustomButton.Visible = false;
            this.fnameText.Lines = new string[0];
            this.fnameText.Location = new System.Drawing.Point(164, 74);
            this.fnameText.MaxLength = 32767;
            this.fnameText.Name = "fnameText";
            this.fnameText.PasswordChar = '\0';
            this.fnameText.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.fnameText.SelectedText = "";
            this.fnameText.SelectionLength = 0;
            this.fnameText.SelectionStart = 0;
            this.fnameText.Size = new System.Drawing.Size(218, 23);
            this.fnameText.Style = MetroFramework.MetroColorStyle.Brown;
            this.fnameText.TabIndex = 2;
            this.fnameText.UseSelectable = true;
            this.fnameText.WaterMark = "Enter your first name";
            this.fnameText.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.fnameText.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroLabel8
            // 
            this.metroLabel8.AutoSize = true;
            this.metroLabel8.Location = new System.Drawing.Point(39, 191);
            this.metroLabel8.Name = "metroLabel8";
            this.metroLabel8.Size = new System.Drawing.Size(56, 19);
            this.metroLabel8.Style = MetroFramework.MetroColorStyle.Brown;
            this.metroLabel8.TabIndex = 9;
            this.metroLabel8.Text = "Address";
            // 
            // metroLabel7
            // 
            this.metroLabel7.AutoSize = true;
            this.metroLabel7.Location = new System.Drawing.Point(698, 123);
            this.metroLabel7.Name = "metroLabel7";
            this.metroLabel7.Size = new System.Drawing.Size(56, 19);
            this.metroLabel7.Style = MetroFramework.MetroColorStyle.Brown;
            this.metroLabel7.TabIndex = 8;
            this.metroLabel7.Text = "Religion";
            // 
            // metroLabel6
            // 
            this.metroLabel6.AutoSize = true;
            this.metroLabel6.Location = new System.Drawing.Point(309, 123);
            this.metroLabel6.Name = "metroLabel6";
            this.metroLabel6.Size = new System.Drawing.Size(71, 19);
            this.metroLabel6.Style = MetroFramework.MetroColorStyle.Brown;
            this.metroLabel6.TabIndex = 7;
            this.metroLabel6.Text = "Civil Status";
            // 
            // metroLabel5
            // 
            this.metroLabel5.AutoSize = true;
            this.metroLabel5.Location = new System.Drawing.Point(173, 123);
            this.metroLabel5.Name = "metroLabel5";
            this.metroLabel5.Size = new System.Drawing.Size(63, 19);
            this.metroLabel5.Style = MetroFramework.MetroColorStyle.Brown;
            this.metroLabel5.TabIndex = 6;
            this.metroLabel5.Text = "Birthdate";
            // 
            // metroLabel4
            // 
            this.metroLabel4.AutoSize = true;
            this.metroLabel4.Location = new System.Drawing.Point(631, 52);
            this.metroLabel4.Name = "metroLabel4";
            this.metroLabel4.Size = new System.Drawing.Size(71, 19);
            this.metroLabel4.Style = MetroFramework.MetroColorStyle.Brown;
            this.metroLabel4.TabIndex = 5;
            this.metroLabel4.Text = "Last Name";
            // 
            // metroLabel3
            // 
            this.metroLabel3.AutoSize = true;
            this.metroLabel3.Location = new System.Drawing.Point(397, 52);
            this.metroLabel3.Name = "metroLabel3";
            this.metroLabel3.Size = new System.Drawing.Size(90, 19);
            this.metroLabel3.Style = MetroFramework.MetroColorStyle.Brown;
            this.metroLabel3.TabIndex = 4;
            this.metroLabel3.Text = "Middle Name";
            // 
            // metroLabel2
            // 
            this.metroLabel2.AutoSize = true;
            this.metroLabel2.Location = new System.Drawing.Point(164, 52);
            this.metroLabel2.Name = "metroLabel2";
            this.metroLabel2.Size = new System.Drawing.Size(73, 19);
            this.metroLabel2.Style = MetroFramework.MetroColorStyle.Brown;
            this.metroLabel2.TabIndex = 3;
            this.metroLabel2.Text = "First Name";
            // 
            // metroLabel1
            // 
            this.metroLabel1.AutoSize = true;
            this.metroLabel1.FontWeight = MetroFramework.MetroLabelWeight.Regular;
            this.metroLabel1.Location = new System.Drawing.Point(39, 16);
            this.metroLabel1.Name = "metroLabel1";
            this.metroLabel1.Size = new System.Drawing.Size(137, 19);
            this.metroLabel1.Style = MetroFramework.MetroColorStyle.Brown;
            this.metroLabel1.TabIndex = 2;
            this.metroLabel1.Text = "Personal Information";
            this.metroLabel1.UseStyleColors = true;
            // 
            // metroTabPage2
            // 
            this.metroTabPage2.AutoScroll = true;
            this.metroTabPage2.Controls.Add(this.viewInfoButton);
            this.metroTabPage2.Controls.Add(this.metroPanel1);
            this.metroTabPage2.Controls.Add(this.metroGrid1);
            this.metroTabPage2.Controls.Add(this.metroTextBox1);
            this.metroTabPage2.HorizontalScrollbar = true;
            this.metroTabPage2.HorizontalScrollbarBarColor = true;
            this.metroTabPage2.HorizontalScrollbarHighlightOnWheel = false;
            this.metroTabPage2.HorizontalScrollbarSize = 10;
            this.metroTabPage2.Location = new System.Drawing.Point(4, 38);
            this.metroTabPage2.Name = "metroTabPage2";
            this.metroTabPage2.Size = new System.Drawing.Size(985, 692);
            this.metroTabPage2.TabIndex = 1;
            this.metroTabPage2.Text = "EDIT MEMBER PROFILE";
            this.metroTabPage2.VerticalScrollbar = true;
            this.metroTabPage2.VerticalScrollbarBarColor = true;
            this.metroTabPage2.VerticalScrollbarHighlightOnWheel = false;
            this.metroTabPage2.VerticalScrollbarSize = 10;
            // 
            // metroGrid1
            // 
            this.metroGrid1.AllowUserToAddRows = false;
            this.metroGrid1.AllowUserToDeleteRows = false;
            this.metroGrid1.AllowUserToResizeRows = false;
            this.metroGrid1.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.metroGrid1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.metroGrid1.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.metroGrid1.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(165)))), ((int)(((byte)(81)))), ((int)(((byte)(0)))));
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(100)))), ((int)(((byte)(0)))));
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.metroGrid1.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.metroGrid1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.metroGrid1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.name,
            this.year,
            this.course});
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(136)))), ((int)(((byte)(136)))), ((int)(((byte)(136)))));
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(100)))), ((int)(((byte)(0)))));
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.metroGrid1.DefaultCellStyle = dataGridViewCellStyle2;
            this.metroGrid1.EnableHeadersVisualStyles = false;
            this.metroGrid1.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.metroGrid1.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.metroGrid1.Location = new System.Drawing.Point(26, 110);
            this.metroGrid1.MultiSelect = false;
            this.metroGrid1.Name = "metroGrid1";
            this.metroGrid1.ReadOnly = true;
            this.metroGrid1.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(165)))), ((int)(((byte)(81)))), ((int)(((byte)(0)))));
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(100)))), ((int)(((byte)(0)))));
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.metroGrid1.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.metroGrid1.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.metroGrid1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.metroGrid1.Size = new System.Drawing.Size(345, 205);
            this.metroGrid1.Style = MetroFramework.MetroColorStyle.Brown;
            this.metroGrid1.TabIndex = 4;
            this.metroGrid1.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.metroGrid1_CellContentClick);
            // 
            // name
            // 
            this.name.HeaderText = "Name";
            this.name.Name = "name";
            this.name.ReadOnly = true;
            // 
            // year
            // 
            this.year.HeaderText = "Year";
            this.year.Name = "year";
            this.year.ReadOnly = true;
            // 
            // course
            // 
            this.course.HeaderText = "Course";
            this.course.Name = "course";
            this.course.ReadOnly = true;
            // 
            // metroTextBox1
            // 
            // 
            // 
            // 
            this.metroTextBox1.CustomButton.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.metroTextBox1.CustomButton.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.metroTextBox1.CustomButton.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image")));
            this.metroTextBox1.CustomButton.Location = new System.Drawing.Point(317, 1);
            this.metroTextBox1.CustomButton.Name = "";
            this.metroTextBox1.CustomButton.Size = new System.Drawing.Size(27, 27);
            this.metroTextBox1.CustomButton.Style = MetroFramework.MetroColorStyle.Brown;
            this.metroTextBox1.CustomButton.TabIndex = 1;
            this.metroTextBox1.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.metroTextBox1.CustomButton.UseCustomBackColor = true;
            this.metroTextBox1.CustomButton.UseCustomForeColor = true;
            this.metroTextBox1.CustomButton.UseSelectable = true;
            this.metroTextBox1.CustomButton.UseStyleColors = true;
            this.metroTextBox1.CustomButton.UseVisualStyleBackColor = true;
            this.metroTextBox1.Icon = ((System.Drawing.Image)(resources.GetObject("metroTextBox1.Icon")));
            this.metroTextBox1.Lines = new string[0];
            this.metroTextBox1.Location = new System.Drawing.Point(26, 56);
            this.metroTextBox1.MaxLength = 32767;
            this.metroTextBox1.Name = "metroTextBox1";
            this.metroTextBox1.PasswordChar = '\0';
            this.metroTextBox1.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.metroTextBox1.SelectedText = "";
            this.metroTextBox1.SelectionLength = 0;
            this.metroTextBox1.SelectionStart = 0;
            this.metroTextBox1.ShowButton = true;
            this.metroTextBox1.ShowClearButton = true;
            this.metroTextBox1.Size = new System.Drawing.Size(345, 29);
            this.metroTextBox1.Style = MetroFramework.MetroColorStyle.Brown;
            this.metroTextBox1.TabIndex = 3;
            this.metroTextBox1.UseSelectable = true;
            this.metroTextBox1.WaterMark = "Search member";
            this.metroTextBox1.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.metroTextBox1.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroTabPage3
            // 
            this.metroTabPage3.AutoScroll = true;
            this.metroTabPage3.HorizontalScrollbar = true;
            this.metroTabPage3.HorizontalScrollbarBarColor = true;
            this.metroTabPage3.HorizontalScrollbarHighlightOnWheel = false;
            this.metroTabPage3.HorizontalScrollbarSize = 10;
            this.metroTabPage3.Location = new System.Drawing.Point(4, 38);
            this.metroTabPage3.Name = "metroTabPage3";
            this.metroTabPage3.Size = new System.Drawing.Size(985, 692);
            this.metroTabPage3.TabIndex = 2;
            this.metroTabPage3.Text = "SEARCH MEMBER PROFILE";
            this.metroTabPage3.VerticalScrollbar = true;
            this.metroTabPage3.VerticalScrollbarBarColor = true;
            this.metroTabPage3.VerticalScrollbarHighlightOnWheel = false;
            this.metroTabPage3.VerticalScrollbarSize = 10;
            // 
            // metroPanel1
            // 
            this.metroPanel1.HorizontalScrollbarBarColor = true;
            this.metroPanel1.HorizontalScrollbarHighlightOnWheel = false;
            this.metroPanel1.HorizontalScrollbarSize = 10;
            this.metroPanel1.Location = new System.Drawing.Point(396, 3);
            this.metroPanel1.Name = "metroPanel1";
            this.metroPanel1.Size = new System.Drawing.Size(593, 511);
            this.metroPanel1.TabIndex = 5;
            this.metroPanel1.VerticalScrollbarBarColor = true;
            this.metroPanel1.VerticalScrollbarHighlightOnWheel = false;
            this.metroPanel1.VerticalScrollbarSize = 10;
            // 
            // viewInfoButton
            // 
            this.viewInfoButton.Location = new System.Drawing.Point(238, 321);
            this.viewInfoButton.Name = "viewInfoButton";
            this.viewInfoButton.Size = new System.Drawing.Size(133, 39);
            this.viewInfoButton.TabIndex = 6;
            this.viewInfoButton.Text = "View User Information";
            this.viewInfoButton.UseSelectable = true;
            //this.viewInfoButton.Click += new System.EventHandler(this.viewInfoButton_Click);
            // 
            // secMem
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.metroTabControl1);
            this.Name = "secMem";
            this.Size = new System.Drawing.Size(993, 734);
            this.Style = MetroFramework.MetroColorStyle.Brown;
            this.UseCustomBackColor = true;
            this.Load += new System.EventHandler(this.panel1_Load);
            this.metroTabControl1.ResumeLayout(false);
            this.metroTabPage1.ResumeLayout(false);
            this.metroTabPage1.PerformLayout();
            this.metroTabPage2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.metroGrid1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private MetroFramework.Controls.MetroTabControl metroTabControl1;
        private MetroFramework.Controls.MetroTabPage metroTabPage1;
        private MetroFramework.Controls.MetroTabPage metroTabPage2;
        private MetroFramework.Controls.MetroTabPage metroTabPage3;
        private MetroFramework.Controls.MetroLabel metroLabel1;
        private MetroFramework.Controls.MetroTextBox addressRichTextBox;
        private MetroFramework.Controls.MetroTextBox relText;
        private MetroFramework.Controls.MetroComboBox civilstatusCBox;
        private MetroFramework.Controls.MetroDateTime bdatePicker;
        private MetroFramework.Controls.MetroTextBox lnameText;
        private MetroFramework.Controls.MetroTextBox mnameText;
        private MetroFramework.Controls.MetroTextBox fnameText;
        private MetroFramework.Controls.MetroLabel metroLabel8;
        private MetroFramework.Controls.MetroLabel metroLabel7;
        private MetroFramework.Controls.MetroLabel metroLabel6;
        private MetroFramework.Controls.MetroLabel metroLabel5;
        private MetroFramework.Controls.MetroLabel metroLabel4;
        private MetroFramework.Controls.MetroLabel metroLabel3;
        private MetroFramework.Controls.MetroLabel metroLabel2;
        private MetroFramework.Controls.MetroComboBox restypeText;
        private MetroFramework.Controls.MetroLabel metroLabel10;
        private MetroFramework.Controls.MetroDateTime startdatePicker;
        private MetroFramework.Controls.MetroLabel metroLabel9;
        private MetroFramework.Controls.MetroComboBox educattainText;
        private MetroFramework.Controls.MetroTextBox emaddText;
        private MetroFramework.Controls.MetroTextBox mobileText;
        private MetroFramework.Controls.MetroTextBox telnoText;
        private MetroFramework.Controls.MetroLabel metroLabel15;
        private MetroFramework.Controls.MetroLabel metroLabel14;
        private MetroFramework.Controls.MetroLabel metroLabel13;
        private MetroFramework.Controls.MetroLabel metroLabel12;
        private MetroFramework.Controls.MetroLabel metroLabel11;
        private MetroFramework.Controls.MetroTextBox housetypeText;
        private MetroFramework.Controls.MetroLabel metroLabel21;
        private MetroFramework.Controls.MetroLabel metroLabel20;
        private MetroFramework.Controls.MetroLabel metroLabel19;
        private MetroFramework.Controls.MetroLabel metroLabel18;
        private MetroFramework.Controls.MetroLabel metroLabel17;
        private MetroFramework.Controls.MetroLabel metroLabel16;
        private MetroFramework.Drawing.Html.HtmlPanel htmlPanel1;
        private MetroFramework.Controls.MetroTextBox busscontactText;
        private MetroFramework.Controls.MetroTextBox busaddrichTextBox;
        private MetroFramework.Controls.MetroTextBox occupText;
        private MetroFramework.Controls.MetroComboBox empCBox;
        private MetroFramework.Controls.MetroComboBox incomeCBox;
        private MetroFramework.Controls.MetroTextBox.MetroTextButton addProfileButton;
        private MetroFramework.Controls.MetroTextBox purokText;
        private MetroFramework.Controls.MetroLabel label1;
        private MetroFramework.Controls.MetroComboBox genderCBox;
        private MetroFramework.Controls.MetroLabel metroLabel22;
        private MetroFramework.Controls.MetroTextBox metroTextBox1;
        private MetroFramework.Controls.MetroGrid metroGrid1;
        private System.Windows.Forms.DataGridViewTextBoxColumn name;
        private System.Windows.Forms.DataGridViewTextBoxColumn year;
        private System.Windows.Forms.DataGridViewTextBoxColumn course;
        private MetroFramework.Controls.MetroPanel metroPanel1;
        private MetroFramework.Controls.MetroButton viewInfoButton;

    }
}
