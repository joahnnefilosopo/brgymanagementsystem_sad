﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MetroFramework.Forms;
using Transitions;

namespace panelTransition
{
    public partial class transitionProvider : MetroFramework.Controls.MetroUserControl
    {
        Form fOwner = null;
        bool _loaded = false;

        #region Events
        public event EventHandler Closed;
        public event EventHandler Shown;

        protected virtual void closed(EventArgs e)
        {
            EventHandler handler = Closed;

            if (handler != null) handler(this, e);
        }

        protected virtual void shown(EventArgs e)
        {
            EventHandler handler = Shown;

            if (handler != null) handler(this, e);
        }
        #endregion
        private transitionProvider()
        {
            InitializeComponent();
        }

        public transitionProvider(Form owner) : this()
        {
            this.Visible = false;
            fOwner = owner;
            owner.Controls.Add(this);
            owner.Resize += owner_Resize;
            this.BringToFront();
            ResizeForm();


        }

        void owner_Resize(object sender, EventArgs e)
        {
            ResizeForm();
        }

        private void ResizeForm()
        {
            this.Width = fOwner.Width;
            this.Height = fOwner.Height - 77;
            this.Location = new Point(_loaded ? 0 : fOwner.Width, 50);
        }

        public void swipe(bool show = true)
        {
            this.Visible = true;
            Transition t = new Transitions.Transition(new TransitionType_EaseInEaseOut(350));
            t.add(this, "Left", show ? 0 : this.Width);
            t.run();

            while (this.Left != (show ? 0 : this.Width))
            {
                Application.DoEvents();
            }

            if (!show)
            {
                closed(new EventArgs());
                fOwner.Resize -= owner_Resize;
                fOwner.Controls.Remove(this);
                this.Dispose();
            }
            else
            {
                _loaded = true;
                ResizeForm();
                shown(new EventArgs());
            }
        }
        private void transitionProvider_Load(object sender, EventArgs e)
        {

        }
    }
}
