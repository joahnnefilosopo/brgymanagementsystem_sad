﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MetroFramework.Interfaces;
using MetroFramework.Controls;
using MetroFramework;
using MySql.Data.MySqlClient;

namespace panelTransition.Panel
{
    public partial class secMem : transitionProvider, IMetroControl
    {
        //**CONNECTION STRING
        string connString = "server=localhost;database=barangay;Persist Security Info=True;User Id=root;password=root";
        public secMem(Form owner) : base(owner)
        {
            InitializeComponent();
        }

        private void panel1_Load(object sender, EventArgs e)
        {
            //dummy data
            string[] row = new string[] { "Sakuragi", "1", "Accounting" };
            metroGrid1.Rows.Add(row);
            string[] row1 = new string[] { "Gori", "4", "IT" };
            metroGrid1.Rows.Add(row1);
            string[] row2 = new string[] { "Rukawa", "2", "Entrep" };
            metroGrid1.Rows.Add(row2);
            string[] row3 = new string[] { "Luffy", "2", "Architecture" };
            metroGrid1.Rows.Add(row3);
            string[] row4 = new string[] { "Aang", "1", "Philosophy" };
            metroGrid1.Rows.Add(row4);

                
        }

        private void metroTabPage1_Click(object sender, EventArgs e)
        {

        }

        private void addProfileButton_Click(object sender, EventArgs e)
        {
            if (purokText.Text == "" || fnameText.Text == "" || mnameText.Text == "" || relText.Text == "" || addressRichTextBox.Text == "" || telnoText.Text == "" || mobileText.Text == "" || housetypeText.Text == "" || emaddText.Text == "" || occupText.Text == "" || busscontactText.Text == "")
            {
                MetroMessageBox.Show(this, "Incomplete! Please fill up the empty fields!", "Empty Fields!", MessageBoxButtons.OK, MessageBoxIcon.Hand);
            }
            else
            {
                MySqlConnection myconn = new MySqlConnection(connString);
                string InsertSQL = "INSERT INTO Member " +
                                    "(firstName, middleName, lastName, birthDate, gender, religion, highEducAttain, purok, civilStatus, emailAdd, occupation, mobileNo, bussAddress, bussContactNo, incomeBranch, employment, regDate)" +
                                    "VALUES " +
                                    "('" + fnameText.Text + "', " +
                                    "'" + mnameText.Text + "', " +
                                    "'" + lnameText.Text + "', " +
                                    "'" + bdatePicker.Value.ToString("yyyy/MM/dd") + "', " +
                                    "'" + genderCBox.SelectedItem + "', " +
                                    "'" + relText.Text + "', " +
                                    "'" + educattainText.Text + "', " +
                                    "'" + purokText.Text + "', " +
                                    "'" + civilstatusCBox.SelectedItem + "', " +
                                    "'" + emaddText.Text + "', " +
                                    "'" + occupText.Text + "', " +
                                    "'" + mobileText.Text + "', " +
                                    "'" + busaddrichTextBox.Text + "', " +
                                    "'" + busscontactText.Text + "', " +
                                    "'" + incomeCBox.SelectedItem + "', " +
                                    "'" + empCBox.SelectedItem + "', " +
                                    "'" + (DateTime.Today).ToString("yyyy/MM/dd") + "') ";

                string SelectSQL = "SELECT count(address) FROM household WHERE address = '" + addressRichTextBox.Text + "' ";

                MySqlCommand select = new MySqlCommand(SelectSQL, myconn);
                MySqlCommand myInsert = new MySqlCommand(InsertSQL, myconn);

                myconn.Open();
                select.ExecuteNonQuery();
                myInsert.ExecuteNonQuery();
                myconn.Close();


                if (SelectSQL == "0")
                {
                    string InsertSQL3 = "INSERT INTO Household" +
                                    "(address, houseType, homeTelNo)" +
                                    "VALUES" +
                                    "('" + addressRichTextBox.Text + "', " +
                                    "'" + housetypeText.Text + "', " +
                                    "'" + telnoText.Text + "') ";



                    string InsertSQL2 = "INSERT INTO HouseResident" +
                                        "(householdID, memberID, residenceType, startDate)" +
                                        "VALUES" +
                                        "((SELECT householdID FROM Household WHERE address = '" + addressRichTextBox.Text + "') ,(SELECT memberID FROM Member WHERE firstName = '" + fnameText.Text + "' AND lastName = '" + lnameText.Text + "'), " +
                                        "'" + restypeText.SelectedItem + "', " +
                                        "'" + startdatePicker.Value.ToString("yyyy/MM/dd") + "')";

                    MySqlCommand myInsert3 = new MySqlCommand(InsertSQL3, myconn);
                    MySqlCommand myInsert2 = new MySqlCommand(InsertSQL2, myconn);
                    myconn.Open();
                    myInsert3.ExecuteNonQuery();
                    myInsert2.ExecuteNonQuery();
                    myconn.Close();

                }

                else
                {
                    string InsertSQL2 = "INSERT INTO HouseResident" +
                                       "(householdID, memberID, residenceType, startDate)" +
                                       "VALUES" +
                                       "((SELECT householdID FROM Household WHERE address = '" + addressRichTextBox.Text + "') ,(SELECT memberID FROM Member WHERE firstName = '" + fnameText.Text + "' AND lastName = '" + lnameText.Text + "'), " +
                                       "'" + restypeText.SelectedItem + "', " +
                                       "'" + startdatePicker.Value.ToString("yyyy/MM/dd") + "')";

                    MySqlCommand myInsert2 = new MySqlCommand(InsertSQL2, myconn);
                    myconn.Open();
                    myInsert2.ExecuteNonQuery();
                    myconn.Close();
                }


                MetroMessageBox.Show(this, "Member successfully added!", "New Member Added!", MessageBoxButtons.OK, MessageBoxIcon.Information);

                fnameText.Text = "";
                mnameText.Text = "";
                lnameText.Text = "";
                relText.Text = "";
                addressRichTextBox.Text = "";
                telnoText.Text = "";
                mobileText.Text = "";
                housetypeText.Text = "";
                emaddText.Text = "";
                occupText.Text = "";
                addressRichTextBox.Text = "";
                busscontactText.Text = "";



                
            }
        }

        private void metroComboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void metroLabel22_Click(object sender, EventArgs e)
        {

        }

        private void metroGrid1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }
    }
}
