﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MetroFramework.Forms;
using MetroFramework.Controls;
using MetroFramework;



namespace panelTransition
{
    public partial class loginForm : MetroForm
    {
        public loginForm()
        {
            InitializeComponent();
        }

        private void loginForm_Load(object sender, EventArgs e)
        {
            
        }

        private void loginButton_Click(object sender, EventArgs e)
        {
            if(userText.Text == "s" && passText.Text == "s")
            {
                secDash n = new secDash();
                this.Hide();
                n.Show();
            }
            else
            {
                MetroMessageBox.Show(this, "Try again with the right credentials.", "Error Login", MessageBoxButtons.OK, MessageBoxIcon.Error, 100);
            }
               
        }

        private void exitButton_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
    }
}
