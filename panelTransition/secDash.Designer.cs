﻿namespace panelTransition
{
    partial class secDash
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(secDash));
            this.metroPanel1 = new MetroFramework.Controls.MetroPanel();
            this.metroPanel3 = new MetroFramework.Controls.MetroPanel();
            this.ppButton = new MetroFramework.Controls.MetroTile();
            this.memberButton = new MetroFramework.Controls.MetroTile();
            this.brgyClearButton = new MetroFramework.Controls.MetroTile();
            this.brgyCaseButton = new MetroFramework.Controls.MetroTile();
            this.logoutLinkButton = new MetroFramework.Controls.MetroLink();
            this.homeLinkButton = new MetroFramework.Controls.MetroLink();
            this.timeLabel = new System.Windows.Forms.Label();
            this.dateLabel = new System.Windows.Forms.Label();
            this.mainFormLabel = new System.Windows.Forms.Label();
            this.metroPanel2 = new MetroFramework.Controls.MetroPanel();
            this.metroPanel1.SuspendLayout();
            this.metroPanel3.SuspendLayout();
            this.metroPanel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // metroPanel1
            // 
            this.metroPanel1.Controls.Add(this.metroPanel3);
            this.metroPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.metroPanel1.HorizontalScrollbarBarColor = true;
            this.metroPanel1.HorizontalScrollbarHighlightOnWheel = false;
            this.metroPanel1.HorizontalScrollbarSize = 10;
            this.metroPanel1.Location = new System.Drawing.Point(20, 60);
            this.metroPanel1.Name = "metroPanel1";
            this.metroPanel1.Size = new System.Drawing.Size(1049, 458);
            this.metroPanel1.TabIndex = 0;
            this.metroPanel1.VerticalScrollbarBarColor = true;
            this.metroPanel1.VerticalScrollbarHighlightOnWheel = false;
            this.metroPanel1.VerticalScrollbarSize = 10;
            // 
            // metroPanel3
            // 
            this.metroPanel3.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.metroPanel3.BackColor = System.Drawing.Color.WhiteSmoke;
            this.metroPanel3.Controls.Add(this.ppButton);
            this.metroPanel3.Controls.Add(this.memberButton);
            this.metroPanel3.Controls.Add(this.brgyClearButton);
            this.metroPanel3.Controls.Add(this.brgyCaseButton);
            this.metroPanel3.HorizontalScrollbarBarColor = true;
            this.metroPanel3.HorizontalScrollbarHighlightOnWheel = false;
            this.metroPanel3.HorizontalScrollbarSize = 10;
            this.metroPanel3.Location = new System.Drawing.Point(328, 45);
            this.metroPanel3.Name = "metroPanel3";
            this.metroPanel3.Size = new System.Drawing.Size(721, 376);
            this.metroPanel3.TabIndex = 8;
            this.metroPanel3.UseCustomBackColor = true;
            this.metroPanel3.VerticalScrollbarBarColor = true;
            this.metroPanel3.VerticalScrollbarHighlightOnWheel = false;
            this.metroPanel3.VerticalScrollbarSize = 10;
            // 
            // ppButton
            // 
            this.ppButton.ActiveControl = null;
            this.ppButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.ppButton.BackColor = System.Drawing.Color.DarkTurquoise;
            this.ppButton.Location = new System.Drawing.Point(362, 190);
            this.ppButton.Name = "ppButton";
            this.ppButton.Size = new System.Drawing.Size(329, 152);
            this.ppButton.TabIndex = 6;
            this.ppButton.Text = "Program/Project Monitoring";
            this.ppButton.TileImage = ((System.Drawing.Image)(resources.GetObject("ppButton.TileImage")));
            this.ppButton.TileImageAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.ppButton.UseCustomBackColor = true;
            this.ppButton.UseSelectable = true;
            this.ppButton.UseTileImage = true;
            // 
            // memberButton
            // 
            this.memberButton.ActiveControl = null;
            this.memberButton.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.memberButton.BackColor = System.Drawing.Color.Tomato;
            this.memberButton.Location = new System.Drawing.Point(27, 31);
            this.memberButton.Name = "memberButton";
            this.memberButton.Size = new System.Drawing.Size(329, 153);
            this.memberButton.TabIndex = 3;
            this.memberButton.Text = "Member Profiling";
            this.memberButton.TileImage = ((System.Drawing.Image)(resources.GetObject("memberButton.TileImage")));
            this.memberButton.TileImageAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.memberButton.UseCustomBackColor = true;
            this.memberButton.UseSelectable = true;
            this.memberButton.UseTileImage = true;
            this.memberButton.Click += new System.EventHandler(this.memberButton_Click);
            // 
            // brgyClearButton
            // 
            this.brgyClearButton.ActiveControl = null;
            this.brgyClearButton.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.brgyClearButton.BackColor = System.Drawing.Color.YellowGreen;
            this.brgyClearButton.Location = new System.Drawing.Point(362, 31);
            this.brgyClearButton.Name = "brgyClearButton";
            this.brgyClearButton.Size = new System.Drawing.Size(329, 153);
            this.brgyClearButton.TabIndex = 4;
            this.brgyClearButton.Text = "Barangay Clearance Request";
            this.brgyClearButton.TileImage = ((System.Drawing.Image)(resources.GetObject("brgyClearButton.TileImage")));
            this.brgyClearButton.TileImageAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.brgyClearButton.UseCustomBackColor = true;
            this.brgyClearButton.UseSelectable = true;
            this.brgyClearButton.UseTileImage = true;
            // 
            // brgyCaseButton
            // 
            this.brgyCaseButton.ActiveControl = null;
            this.brgyCaseButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.brgyCaseButton.BackColor = System.Drawing.Color.MediumPurple;
            this.brgyCaseButton.Location = new System.Drawing.Point(27, 190);
            this.brgyCaseButton.Name = "brgyCaseButton";
            this.brgyCaseButton.Size = new System.Drawing.Size(329, 152);
            this.brgyCaseButton.TabIndex = 5;
            this.brgyCaseButton.Text = "Barangay Cases";
            this.brgyCaseButton.TileImage = ((System.Drawing.Image)(resources.GetObject("brgyCaseButton.TileImage")));
            this.brgyCaseButton.TileImageAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.brgyCaseButton.UseCustomBackColor = true;
            this.brgyCaseButton.UseSelectable = true;
            this.brgyCaseButton.UseTileImage = true;
            // 
            // logoutLinkButton
            // 
            this.logoutLinkButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.logoutLinkButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.logoutLinkButton.FontSize = MetroFramework.MetroLinkSize.Tall;
            this.logoutLinkButton.FontWeight = MetroFramework.MetroLinkWeight.Light;
            this.logoutLinkButton.Location = new System.Drawing.Point(991, 15);
            this.logoutLinkButton.Name = "logoutLinkButton";
            this.logoutLinkButton.Size = new System.Drawing.Size(78, 39);
            this.logoutLinkButton.TabIndex = 3;
            this.logoutLinkButton.Text = "Logout";
            this.logoutLinkButton.UseSelectable = true;
            this.logoutLinkButton.Click += new System.EventHandler(this.logoutLinkButton_Click);
            // 
            // homeLinkButton
            // 
            this.homeLinkButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.homeLinkButton.FontSize = MetroFramework.MetroLinkSize.Tall;
            this.homeLinkButton.FontWeight = MetroFramework.MetroLinkWeight.Light;
            this.homeLinkButton.Location = new System.Drawing.Point(907, 15);
            this.homeLinkButton.Name = "homeLinkButton";
            this.homeLinkButton.Size = new System.Drawing.Size(78, 39);
            this.homeLinkButton.TabIndex = 4;
            this.homeLinkButton.Text = "Home";
            this.homeLinkButton.UseSelectable = true;
            this.homeLinkButton.Visible = false;
            this.homeLinkButton.Click += new System.EventHandler(this.homeLinkButton_Click);
            // 
            // timeLabel
            // 
            this.timeLabel.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.timeLabel.AutoSize = true;
            this.timeLabel.BackColor = System.Drawing.Color.Transparent;
            this.timeLabel.Font = new System.Drawing.Font("Segoe UI Light", 15F);
            this.timeLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.timeLabel.Location = new System.Drawing.Point(3, 346);
            this.timeLabel.Name = "timeLabel";
            this.timeLabel.Size = new System.Drawing.Size(86, 28);
            this.timeLabel.TabIndex = 2;
            this.timeLabel.Text = "12:30 PM";
            this.timeLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // dateLabel
            // 
            this.dateLabel.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.dateLabel.AutoSize = true;
            this.dateLabel.BackColor = System.Drawing.Color.Transparent;
            this.dateLabel.Font = new System.Drawing.Font("Segoe UI Light", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dateLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.dateLabel.Location = new System.Drawing.Point(3, 318);
            this.dateLabel.Name = "dateLabel";
            this.dateLabel.Size = new System.Drawing.Size(194, 28);
            this.dateLabel.TabIndex = 3;
            this.dateLabel.Text = "Monday, June 12, 2015";
            this.dateLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // mainFormLabel
            // 
            this.mainFormLabel.AutoSize = true;
            this.mainFormLabel.BackColor = System.Drawing.Color.Transparent;
            this.mainFormLabel.Font = new System.Drawing.Font("Segoe UI Light", 18F);
            this.mainFormLabel.Location = new System.Drawing.Point(20, 20);
            this.mainFormLabel.Name = "mainFormLabel";
            this.mainFormLabel.Size = new System.Drawing.Size(227, 32);
            this.mainFormLabel.TabIndex = 6;
            this.mainFormLabel.Text = "Secretary Main Panel";
            // 
            // metroPanel2
            // 
            this.metroPanel2.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.metroPanel2.BackColor = System.Drawing.Color.Silver;
            this.metroPanel2.Controls.Add(this.dateLabel);
            this.metroPanel2.Controls.Add(this.timeLabel);
            this.metroPanel2.HorizontalScrollbarBarColor = true;
            this.metroPanel2.HorizontalScrollbarHighlightOnWheel = false;
            this.metroPanel2.HorizontalScrollbarSize = 10;
            this.metroPanel2.Location = new System.Drawing.Point(20, 105);
            this.metroPanel2.Name = "metroPanel2";
            this.metroPanel2.Size = new System.Drawing.Size(298, 376);
            this.metroPanel2.Style = MetroFramework.MetroColorStyle.Blue;
            this.metroPanel2.TabIndex = 9;
            this.metroPanel2.UseCustomBackColor = true;
            this.metroPanel2.VerticalScrollbarBarColor = true;
            this.metroPanel2.VerticalScrollbarHighlightOnWheel = false;
            this.metroPanel2.VerticalScrollbarSize = 10;
            // 
            // secDash
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.logoutLinkButton;
            this.ClientSize = new System.Drawing.Size(1089, 538);
            this.ControlBox = false;
            this.Controls.Add(this.metroPanel2);
            this.Controls.Add(this.mainFormLabel);
            this.Controls.Add(this.homeLinkButton);
            this.Controls.Add(this.logoutLinkButton);
            this.Controls.Add(this.metroPanel1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MinimumSize = new System.Drawing.Size(627, 538);
            this.Name = "secDash";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.metroPanel1.ResumeLayout(false);
            this.metroPanel3.ResumeLayout(false);
            this.metroPanel2.ResumeLayout(false);
            this.metroPanel2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private MetroFramework.Controls.MetroPanel metroPanel1;
        private MetroFramework.Controls.MetroTile brgyClearButton;
        private MetroFramework.Controls.MetroLink logoutLinkButton;
        private MetroFramework.Controls.MetroLink homeLinkButton;
        private System.Windows.Forms.Label dateLabel;
        private System.Windows.Forms.Label timeLabel;
        private MetroFramework.Controls.MetroPanel metroPanel3;
        private MetroFramework.Controls.MetroTile ppButton;
        private MetroFramework.Controls.MetroTile memberButton;
        private MetroFramework.Controls.MetroTile brgyCaseButton;
        private System.Windows.Forms.Label mainFormLabel;
        private MetroFramework.Controls.MetroPanel metroPanel2;
    }
}

